#!/bin/bash

# © 2020, Midgard
# License: GPL-3.0-or-later
# This file is not available under the LGPL!

cd $(dirname "$0")/..

rm -rf ./build/ ./ipo.egg-info/ ./dist/ ./__pycache__/ ./ipo/__pycache__/
